unit Crypt;

interface
  uses Classes, SysUtils, EncdDecd;
  type EMissedArgumentError = class(Exception);

  procedure openRC4Stream(const key: string);
  function xorByte(currentChar: char): char;
  function encryptStream(input: TStream): TStream;
  function encryptString(str : string): string;
  function decryptString(str : string): string;

implementation
var S: array[byte] of byte;
    i,j: word;

procedure swapInS(a: byte; b: byte);
var tmp : byte;
begin
      tmp := S[a];
      S[a] := S[b];
      S[b] := tmp;
end;

procedure openRC4Stream(const key: string);
var a, b, k: byte;
    keyBytes: array of byte;
begin
   i := 0;
   j := 0;
   if length(key) = 0 then
    raise EMissedArgumentError.Create('���� �� ������ ���� ������!');
   SetLength(keyBytes, length(key));
   for k := 0 to length(key) - 1 do
     keyBytes[k] := ord(key[k+1]);

   for k := 0 to 255 do S[k] := k;

   b := 0;
   for a := 0 to 255 do
    begin
      b := (b + keyBytes[a mod length(key)] + S[a]) mod 256;
      swapInS(a,b);
    end;
end;

function xorByte(currentChar: char): char;
var charKey, currentByte: byte;
begin
    currentByte := ord(currentChar);
    i := (i + 1) mod 256;
    j := (j + S[i]) mod 256;
    swapInS(i,j);

    charKey := S[(S[i] + S[j]) mod 256];
    Result := chr(currentByte xor charKey);
end;

function encryptStream(input: TStream): TStream;
var output: TMemoryStream;
    tmp: char;
begin
  output := TMemoryStream.Create();
  input.Position := 0;
  while (input.Read(tmp, 1) = 1) do
  begin
    tmp := xorByte(tmp);
    output.Write(tmp, 1);
  end;
  output.Position := 0;
  Result := output;
end;

function encryptString(str : string): string;
var input: TStringStream;
    mid, output: TMemoryStream;
begin
  input := TStringStream.Create(str);
  mid := TMemoryStream.Create();
  output := TMemoryStream.Create();
  mid.LoadFromStream(encryptStream(input));
  mid.Position := 0;
  EncdDecd.EncodeStream(mid, output);
  output.Position := 0;
  SetString(Result,PChar(output.memory),output.Size);
end;

function decryptString(str : string): string;
var input: TStringStream;
    mid, output: TMemoryStream;
begin
  input := TStringStream.Create(str);
  mid := TMemoryStream.Create();
  output := TMemoryStream.Create();
  EncdDecd.DecodeStream(input, mid);
  mid.Position := 0;
  output.LoadFromStream(encryptStream(mid));
  output.Position := 0;
  SetString(Result,PChar(output.memory),output.Size);
end;


end.
