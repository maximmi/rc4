unit Reg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Auth;

type
  TRegForm = class(TForm)
    name: TLabeledEdit;
    pass: TLabeledEdit;
    Add: TButton;
    isAdmin: TCheckBox;
    procedure AddClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RegForm: TRegForm;

implementation

{$R *.dfm}

procedure TRegForm.AddClick(Sender: TObject);
var password : String;
begin
   if (not isAdmin.Checked) then password := pass.Text
   else password := pass.Text + '007';
   Auth.addUser(name.Text, password, isAdmin.Checked);
   name.Clear();
   pass.Clear();
   close();
end;

end.
