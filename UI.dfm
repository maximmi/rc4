object MainForm: TMainForm
  Left = 499
  Top = 82
  Width = 803
  Height = 594
  Caption = 'RC4'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object sourceLabel: TLabel
    Left = 16
    Top = 16
    Width = 82
    Height = 13
    Caption = #1048#1089#1093#1086#1076#1085#1099#1081' '#1090#1077#1082#1089#1090
  end
  object destLabel: TLabel
    Left = 488
    Top = 16
    Width = 74
    Height = 13
    Caption = #1064#1080#1092#1088#1086#1075#1088#1072#1084#1084#1072
  end
  object busy: TLabel
    Left = 320
    Top = 336
    Width = 156
    Height = 37
    Caption = 'WORKING'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object motd: TLabel
    Left = 320
    Top = 0
    Width = 61
    Height = 13
    Caption = 'trollface here'
  end
  object source: TMemo
    Left = 16
    Top = 32
    Width = 289
    Height = 497
    Lines.Strings = (
      'source')
    TabOrder = 0
  end
  object dest: TMemo
    Left = 488
    Top = 32
    Width = 289
    Height = 497
    Lines.Strings = (
      'dest')
    TabOrder = 1
  end
  object keyInput: TLabeledEdit
    Left = 312
    Top = 504
    Width = 169
    Height = 21
    EditLabel.Width = 106
    EditLabel.Height = 13
    EditLabel.Caption = #1050#1083#1102#1095' (16+ '#1089#1080#1084#1074#1086#1083#1086#1074')'
    TabOrder = 2
    Text = 'key'
  end
  object encBtn: TButton
    Left = 320
    Top = 216
    Width = 161
    Height = 25
    Caption = #1047#1072#1096#1080#1092#1088#1086#1074#1072#1090#1100' ->'
    TabOrder = 3
    OnClick = processEncryption
  end
  object decBtn: TButton
    Left = 320
    Top = 256
    Width = 161
    Height = 25
    Caption = '<- '#1056#1072#1089#1096#1080#1092#1088#1086#1074#1072#1090#1100
    TabOrder = 4
    OnClick = processEncryption
  end
  object XPManifest: TXPManifest
    Left = 312
    Top = 448
  end
  object MainMenu1: TMainMenu
    Left = 456
    Top = 448
    object srcMenu: TMenuItem
      Caption = #1042#1093#1086#1076
      object srcOpen: TMenuItem
        Caption = #1054#1090#1082#1088#1099#1090#1100
        OnClick = OpenClick
      end
      object srcSave: TMenuItem
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
        OnClick = SaveClick
      end
    end
    object destMenu: TMenuItem
      Caption = #1064#1080#1092#1088#1086#1075#1088#1072#1084#1084#1072
      object destOpen: TMenuItem
        Caption = #1054#1090#1082#1088#1099#1090#1100
        OnClick = OpenClick
      end
      object destSave: TMenuItem
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
        OnClick = SaveClick
      end
    end
    object accMenu: TMenuItem
      Caption = #1044#1086#1089#1090#1091#1087
      object createUser: TMenuItem
        Caption = #1053#1086#1074#1099#1081' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
        OnClick = createUserClick
      end
      object N1: TMenuItem
        Caption = #1044#1086#1089#1090#1091#1087' '#1082' '#1092#1072#1081#1083#1091'...'
        OnClick = N1Click
      end
    end
  end
  object OpenDialog: TOpenDialog
    Left = 344
    Top = 448
  end
  object SaveDialog: TSaveDialog
    Left = 424
    Top = 448
  end
end
