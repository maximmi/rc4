unit Rights_management;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBClient, Grids, DBGrids, StdCtrls, Spin;

  type User = record
     id: word;
     name: String;
     rights: byte
  end;

type
  TSelectUsers = class(TForm)
    ClientDataSet1: TClientDataSet;
    Apply: TButton;
    ClientDataSet1user: TStringField;
    ClientDataSet1hash: TStringField;
    ClientDataSet1isAdmin: TBooleanField;
    users: TGroupBox;
    procedure FormShow(Sender: TObject);
    procedure ApplyClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure updateRights(Sender: TObject);
    function createUserForm(id: word; rights: byte): User;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  header: String;
  fileUsers, allUsers: TList;
  currentUser: User;
  SelectUsers: TSelectUsers;
  edits: Array of TSpinEdit;
  function pendal(): byte;
  procedure refreshUsers();
  procedure makeHeader();
  procedure clear();

implementation

{$R *.dfm}

function split(Delimiter: Char; Str: string): TStrings;
var
  i: Integer;
  row: string;
begin
  Result := TStringList.Create;
  i := 1;
  row := '';
  while i <= Length(Str) do
  begin
    if (Str[i] = Delimiter) then
    begin
      Result.Add(row);
      row := '';
    end
    else
      row := row + Str[i];
    i := i + 1;
  end;
  if (row <> '') then
    Result.Add(row);
end;

procedure parseHeader();
var usser: ^User;
    headerParts: TStrings;
    i: word;
begin
  FileUsers.Clear();
  headerParts := split(':', header);
  for i := 0 to (headerParts.Count-1) div 2 do begin
    New(usser);
    usser.id := i;
    usser.name := headerParts[i*2];
    usser.rights := strtoint(headerParts[i*2+1]);
    FileUsers.add(usser);
  end;
end;

function pendal(): byte;
var i: integer;
    rak: ^User;
begin
   Result := 0;
   parseHeader();
   for i := 0 to FileUsers.Count-1 do begin
      rak := FileUsers[i];
      if rak.name = currentUser.name then
        Result := rak.rights;
   end;
end;

procedure kick_something();
begin
  //nop
end;

procedure TSelectUsers.updateRights(Sender: TObject);
var thisFuckinUser : ^User;
begin
  with Sender as TSpinEdit do begin
    if Text <> '' then begin
      thisFuckinUser := allUsers[Tag];
      thisFuckinUser.rights := Value;
    end;
  end;
end;

function TSelectUsers.createUserForm(id: word; rights: byte): User;
var username: TLabel;
    rightsEdit: TSpinEdit;
    top: word;
    usser : User;
begin
  usser.id := id;
  usser.name := ClientDataSet1user.Value;
  usser.rights := rights;
  top := id*24+16;
  username := TLabel.Create(users);
  username.Caption := usser.name;
  username.Tag := id;
  username.Parent := users;
  username.Top := top;
  username.Left := 16;
  rightsEdit := TSpinEdit.Create(users);
  rightsEdit.Tag := id;
  rightsEdit.Top := top;
  rightsEdit.Left := 100;
  rightsEdit.MaxValue := 7;
  rightsEdit.MinValue := 0;
  rightsEdit.Width := 50;
  rightsEdit.Value := usser.rights;
  rightsEdit.Parent := users;
  rightsEdit.OnChange := updateRights;
  edits[id] := rightsEdit;
  Result := usser;
end;

procedure refreshUsers();
var j, rights: word;
    usser: ^User;
begin
  AllUsers.Clear();
  j := 0;
  header := '';
  rights := 0;
  with SelectUsers do begin
  ClientDataSet1.First();
  SetLength(edits, ClientDataSet1.RecordCount);
  while not ClientDataSet1.EOF do begin
    if (ClientDataSet1user.Value = currentUser.name) and (fileUsers.Count = 0)
    then begin
      rights := 7;
      currentUser.id := j;
      fileUsers.add(@currentUser);
    end;
    New(usser);
    usser^ := createUserForm(j, rights);
    allUsers.Add(usser);
    inc(j);
    ClientDataSet1.Next();
  end;
  end;
end;

procedure clear();
begin
  AllUsers.Clear();
  FileUsers.Clear();
end;

procedure TSelectUsers.FormShow(Sender: TObject);
var i: word;
    usser: ^User;
begin
  refreshUsers();
  for i := 0 to FileUsers.Count-1 do begin
    usser := fileUsers[i];
    edits[usser.id].Value := usser.rights;
  end;
end;

procedure makeHeader();
var i: integer;
    thisFuckinUser: ^User;
begin
  header := '';
  fileUsers.Clear();
  for i := 0 to allUsers.Count-1 do begin
      thisFuckinUser := allUsers[i];
      header := header + thisFuckinUser.name+':'+inttostr(thisFuckinUser.rights)+':';
      fileUsers.Add(thisFuckinUser);
  end;
  delete(header, length(header), 1);
end;

procedure TSelectUsers.ApplyClick(Sender: TObject);
begin
  makeHeader();
  close();
end;

procedure TSelectUsers.FormCreate(Sender: TObject);
begin
  fileUsers := TList.Create;
  allUsers := TList.Create;
end;

end.
