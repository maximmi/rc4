object AuthForm: TAuthForm
  Left = 730
  Top = 411
  Width = 154
  Height = 181
  Caption = 'AuthForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object login: TLabeledEdit
    Left = 16
    Top = 24
    Width = 121
    Height = 21
    EditLabel.Width = 31
    EditLabel.Height = 13
    EditLabel.Caption = #1051#1086#1075#1080#1085
    TabOrder = 0
  end
  object pass: TLabeledEdit
    Left = 16
    Top = 72
    Width = 121
    Height = 21
    EditLabel.Width = 38
    EditLabel.Height = 13
    EditLabel.Caption = #1055#1072#1088#1086#1083#1100
    PasswordChar = 'X'
    TabOrder = 1
  end
  object btnLogin: TButton
    Left = 16
    Top = 104
    Width = 121
    Height = 25
    Caption = #1042#1086#1081#1090#1080
    TabOrder = 2
    OnClick = btnLoginClick
  end
  object ClientDataSet1: TClientDataSet
    Active = True
    Aggregates = <>
    FileName = 'C:\Documents and Settings\Masya\'#1056#1072#1073#1086#1095#1080#1081' '#1089#1090#1086#1083'\RC4\pass.cds'
    Params = <>
    Left = 16
    Top = 136
    Data = {
      180100009619E0BD010000001800000003000300000003000000930004757365
      7201004900000001000557494454480200020020000468617368010049000000
      010005574944544802000200400007697341646D696E02000300000000000100
      0A4348414E47455F4C4F47040082000900000001000000000000000400000002
      00000000000000040000000300000000000000040000000400086330306C7573
      6572203439383244423739413646364338424531343039343032303134303732
      374238010004000A7363687469726C69747A2041464438374241434531443941
      4242463935373233334233374338454130393100000400013120413030453545
      42303937334432343634394134413932304643353344393536340100}
    object ClientDataSet1user: TStringField
      DisplayWidth = 19
      FieldName = 'user'
      Size = 32
    end
    object ClientDataSet1hash: TStringField
      DisplayWidth = 36
      FieldName = 'hash'
      Size = 64
    end
    object ClientDataSet1isAdmin: TBooleanField
      DisplayWidth = 7
      FieldName = 'isAdmin'
    end
  end
end
