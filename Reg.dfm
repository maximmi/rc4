object RegForm: TRegForm
  Left = 222
  Top = 226
  Width = 168
  Height = 163
  Caption = 'RegForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object name: TLabeledEdit
    Left = 24
    Top = 32
    Width = 121
    Height = 21
    EditLabel.Width = 26
    EditLabel.Height = 13
    EditLabel.Caption = 'name'
    TabOrder = 0
  end
  object pass: TLabeledEdit
    Left = 24
    Top = 72
    Width = 121
    Height = 21
    EditLabel.Width = 22
    EditLabel.Height = 13
    EditLabel.Caption = 'pass'
    TabOrder = 1
  end
  object Add: TButton
    Left = 24
    Top = 96
    Width = 121
    Height = 25
    Caption = 'Add'
    TabOrder = 2
    OnClick = AddClick
  end
  object isAdmin: TCheckBox
    Left = 72
    Top = 8
    Width = 73
    Height = 17
    Caption = 'isAdmin'
    TabOrder = 3
  end
end
