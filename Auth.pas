unit Auth;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, UI, IdGlobal, IdHash, IdHashMessageDigest,
  DB, DBClient, Grids, DBGrids;

type
  TAuthForm = class(TForm)
    login: TLabeledEdit;
    pass: TLabeledEdit;
    btnLogin: TButton;
    ClientDataSet1: TClientDataSet;
    ClientDataSet1user: TStringField;
    ClientDataSet1hash: TStringField;
    ClientDataSet1isAdmin: TBooleanField;
    procedure btnLoginClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AuthForm: TAuthForm;
  procedure addUser(logn: String; passwd: String; isAdmin: boolean);

implementation

{$R *.dfm}

function md5(pass: String): String;
begin
  with TIdHashMessageDigest5.Create do
    try
      Result := TIdHash128.AsHex(HashValue(pass));
    finally
      Free;
  end;
end;

procedure addUser(logn: String; passwd: String; isAdmin: boolean);
begin
with AuthForm.ClientDataSet1 do begin
    Insert;
    Fields.Fields[0].AsVariant:=logn;
    Fields.Fields[1].AsVariant:=md5(passwd);
    Fields.Fields[2].AsVariant:=isAdmin;
    Post;
end;
end;

procedure TAuthForm.btnLoginClick(Sender: TObject);
var password: String;
begin
  if (ClientDataSet1.Locate('user', login.Text, [])) then begin
    password := pass.Text;
    if (ClientDataSet1isAdmin.Value) then password := password + '007';
    if (ClientDataSet1hash.Value = md5(password)) then begin
      MainForm.setUser(ClientDataSet1user.Value, ClientDataSet1isAdmin.Value);
      MainForm.ShowModal();
    end else showmessage('��� �� ��� ������');;
  end else showmessage('�� ���');
end;

end.
