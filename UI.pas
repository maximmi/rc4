unit UI;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, XPMan, ExtCtrls, StdCtrls, Menus, Crypt, EncdDecd, DB, DBClient;

type
  TMainForm = class(TForm)
    source: TMemo;
    dest: TMemo;
    sourceLabel: TLabel;
    destLabel: TLabel;
    keyInput: TLabeledEdit;
    XPManifest: TXPManifest;
    encBtn: TButton;
    decBtn: TButton;
    MainMenu1: TMainMenu;
    srcMenu: TMenuItem;
    destMenu: TMenuItem;
    srcOpen: TMenuItem;
    srcSave: TMenuItem;
    destOpen: TMenuItem;
    destSave: TMenuItem;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    busy: TLabel;
    motd: TLabel;
    accMenu: TMenuItem;
    createUser: TMenuItem;
    N1: TMenuItem;
    procedure OpenClick(Sender: TObject);
    procedure OpenFile(Memo: TMemo);
    procedure SaveClick(Sender: TObject);
    procedure SaveFile(Memo: TMemo);
    procedure processEncryption(Sender: TObject);
    procedure setUser(username: string; isAdmin: boolean);
    procedure createUserClick(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

const MASTERKEY = 'My6Super465Puper8768Mega25Key24353654';

implementation

uses Reg, Rights_management;

{$R *.dfm}

procedure TMainForm.OpenFile(Memo: TMemo);
begin
  if (OpenDialog.Execute()) then
  begin
    Memo.Lines.LoadFromFile(OpenDialog.FileName);
  end;
end;

procedure TMainForm.OpenClick(Sender: TObject);
begin
   if (Sender = srcOpen) then
      OpenFile(source);
   if (Sender = destOpen) then begin
      OpenFile(dest);
      Crypt.openRC4Stream(MASTERKEY);
      Rights_management.header := Crypt.decryptString(dest.Lines[0]);
      dest.Lines.Delete(0);
      if (Rights_management.pendal() and 4) = 0 then begin
        dest.Clear();
        dest.Lines.Append('Access denied!');
      end;
      if (Rights_management.pendal() and 2) = 0 then begin
        source.ReadOnly := true;
        dest.ReadOnly := true;
        dest.Enabled := false;
      end else begin
        source.ReadOnly := false;
        dest.ReadOnly := false;
        dest.Enabled := true;
      end;
      if (Rights_management.pendal() and 1) = 0 then begin
        N1.Visible := false;
      end else begin
        N1.Visible := true;
      end;
   end;
end;

procedure TMainForm.SaveFile(Memo: TMemo);
begin
  if (SaveDialog.Execute()) then
  begin
    Memo.Lines.SaveToFile(SaveDialog.FileName);
  end;
end;

procedure TMainForm.SaveClick(Sender: TObject);
begin
   if (Sender = srcSave) then
      SaveFile(source);
   if (Sender = destSave) then begin
      Rights_management.makeHeader();
      Crypt.openRC4Stream(MASTERKEY);
      dest.Lines.Insert(0, Crypt.encryptString(Rights_management.header));
      SaveFile(dest);
      dest.Lines.Delete(0);
   end;
end;

procedure TMainForm.processEncryption(Sender: TObject);
var input, base64: TMemoryStream;
begin
  busy.Show();
  MainForm.Repaint();
  input := TMemoryStream.Create();
  base64 := TMemoryStream.Create();
  Crypt.openRC4Stream(keyInput.Text);
  if (Sender = encBtn) then begin
    source.Lines.SaveToStream(input);
    EncdDecd.EncodeStream(Crypt.encryptStream(input), base64);
    base64.Position := 0;
    dest.Lines.LoadFromStream(base64);
  end else if (Sender = decBtn) then
  begin
    dest.Lines.SaveToStream(base64);
    base64.Position := 0;
    EncdDecd.DecodeStream(base64, input);
    source.Lines.LoadFromStream(Crypt.encryptStream(input));
  end;
  input.Free();
  base64.Free();
  busy.Hide();
end;

procedure TMainform.setUser(username: string; isAdmin: boolean);
begin
  motd.Caption := 'Welcome back, agent '+username;
  Rights_management.currentUser.name := username;
  Rights_management.currentUser.rights := 7;
  decBtn.Visible := isAdmin;
  accMenu.Visible := isAdmin;
end;

procedure TMainForm.createUserClick(Sender: TObject);
begin
  RegForm.showModal();
end;

procedure TMainForm.N1Click(Sender: TObject);
begin
  SelectUsers.showModal();
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  Rights_management.refreshUsers;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  source.Clear();
  dest.Clear();
  Rights_management.Clear();
end;

end.
