program RC4;

uses
  Forms,
  UI in 'UI.pas' {MainForm},
  Crypt in 'Crypt.pas',
  Auth in 'Auth.pas' {AuthForm},
  Reg in 'Reg.pas' {RegForm},
  Rights_management in 'Rights_management.pas' {SelectUsers};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TAuthForm, AuthForm);
  Application.CreateForm(TRegForm, RegForm);
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TSelectUsers, SelectUsers);
  Application.Run;
end.
